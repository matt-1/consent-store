# Consent Store
Simple GCP serverless system to store user consent, e.g. recording user consent
for cookies for the GDPR.

# Important!
This **has not been reviewed for compliance** by any lawyers or legal experts.
It is just based on my own understanding of the requirements of the GDPR.

If you want to use this, please have a lawyer or legal expert review the code.

# Local Dev

Start the local [GCF emulator](https://cloud.google.com/functions/docs/emulator)

```functions start```

Then deploy the functions:

```functions deploy postConsent --trigger-http --local-path=.```

You can then call the function directly using

```functions call postConsent```

# Deploying to server

Commit changes and push to master on Github.  Google Source repo will automatically get changes and use it during deploy:

```gcloud beta functions deploy postConsent --region europe-west1 --memory 128 --runtime nodejs8  --trigger-http --source https://source.developers.google.com/projects/consent-store/repos/consent-store/moveable-aliases/master/paths/```

N.B. we need to use `beta` as node 8 is not yet GA.