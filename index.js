const Datastore = require('@google-cloud/datastore');
const crypto = require('crypto');
const projectId = 'consent-store';

const datastore = new Datastore({
    projectId: projectId,
});

// Store the consent from the user.
exports.postConsent = async (req, res) => {
    const kind = 'consent';
    const entityKey = datastore.key([kind]);
    const now = new Date();
    const consent = {
        key: entityKey,
        data: {
            consent: true,
            consentIpHash: crypto.createHash('sha256').update(req.ip).digest('hex'),
            given: now.getTime(),
            expires: now.setUTCFullYear(now.getUTCFullYear() + 1),
        }
    };

    await datastore.save(consent).catch(e => {
        console.log('Error saving consent entity: ' + e);
        res.status(500).send('error');
        return;
    });
  
    console.log('Saved consent ok.');
    res.status(200).send('ok');
};